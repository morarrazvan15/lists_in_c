#include<stdio.h>
#include<malloc.h>
#include<string.h>
#include<iostream>
/*
SIMPLE LINKED LIST
DOUBLE LINKED LIST
QUEUE PRINT
RINGBUFFER
INSERT AT THE BEG,END;-> SIMPLE LIST
DELETE FROM BEG, END -> DOUBLE LIST

PRINT AND DELETE AT THE SAME TIME

HASHTABLE + INSERT+ SEARCH


*/

/*Struct Product , create A product, Print a product*/

/* There are two possible ways to create lists
1-> You can create a Struct that holds a pointer to the begining at the list, and everytime you do anything on that list, you change inside the structure
2-> you just work with *head and change it everytime

I've done both methods so choose which one you like most:);

*/
struct Product
{
	int id;
	char* name;
	float price;
};

Product InitProduct(int id, const char* name, float price)
{
	Product newProduct;
	newProduct.id = id;
	newProduct.name = new char[strlen(name)];
	strcpy(newProduct.name, name);
	newProduct.price = price;
	return newProduct;
}

void PrintProduct(Product p)
{
	printf("Id:%d -> Name: ->%s Price:%.2f\n", p.id,p.name,p.price);
	
}
/* Simple Linked List + Operations on List*/

struct node
{
	Product p;
	node *next;
};

node* createNode(Product p)
{
	node* newNode = (node*)malloc(sizeof(node));
	newNode->p.id = p.id;
	newNode->p.name = new char[strlen(p.name)];
	strcpy(newNode->p.name, p.name);
	newNode->p.price = p.price;
	newNode->next = NULL;
	return newNode;
}

struct SimpleLinkedList 
{
	node* head;
};

SimpleLinkedList createList()
{
	SimpleLinkedList list;
	list.head = NULL;
	return list;
}

void InsertElementAtTheEnd(SimpleLinkedList &list, Product p)
{
	node* newNode = createNode(p);
	if (list.head == NULL)
	{
		list.head = newNode;
	
	}
	else
	{
		node* iterator = list.head;
		while (iterator->next != NULL)

		{
			iterator = iterator->next;
		}
		iterator->next = newNode;
		
	}
}
void insertElementAtTheBegging(SimpleLinkedList &list, Product p)
{
	node*newNode = createNode(p);
	if (list.head == NULL)
	{
		list.head = newNode;
	}
	else
	{
		newNode->next = list.head;
		list.head = newNode;
	}

	
}
void deleteEleementAtTheBegging(SimpleLinkedList &list)
{
	if (list.head == NULL)
	{
		printf("\nNothing to delete");
	}
	else
	{
		node* element = list.head;
		list.head = list.head->next;
		free(element);
	}
			
		
}
void deleteEmenetAtTheEnd(SimpleLinkedList &list)
{
	if (list.head == NULL)
	{
		printf("\nNothing to delete");
	}
	else if (list.head->next == NULL)
	{
		free(list.head);
		list.head = NULL;
	}
	else
	{
		node* iterator = list.head;
		while (iterator->next->next != NULL)
		{
			iterator = iterator->next;
		}
		node* element = iterator->next;
		free(element);
		iterator->next = NULL;
	}
}

void PrintList(SimpleLinkedList List)
{
	while (List.head != NULL)
	{
		PrintProduct(List.head->p);
		List.head = List.head->next;
	}
}


void InsertElementAtTheEndWith(node* &head, Product p)
{
	node* newNode = createNode(p);
	if (head == NULL)
	{
		head = newNode;
	}
	else
	{
		
		node* iterator = head;
		while (iterator->next != NULL)
		{
			iterator = iterator->next;
		}
		iterator->next = newNode;
		

	}
}
void InsertElementAtTheBeginingWith(node* &head, Product p)
{
	node* element = createNode(p);
	if (head == NULL)

	{
		head = element;
	}
	else
	{
		element->next = head;
		head = element;
	}
}

void DeleteElementAtTheBeginingWith(node* &head)
{
	if (head == NULL)
	{
		printf("\n Nothing to delete");
	}
	else
	{
		node* element = head;
		head = head->next;
		
		free(element);
	}
}
void DeleteElementAtTheEndWith(node*&head)
{
	if (head == NULL)
	{
		printf("\n Nothing to delete");
	}
	else if (head->next == NULL)
	{
		free(head);
		head = NULL;

	}
	else
	{
		node* iterator = head;
		while (iterator->next->next!=NULL)
		{
			iterator = iterator->next;
		}
		node*element = iterator->next;
		free(element);
		iterator->next = NULL;

	}

}
void printListMethod2(node*head)
{
	if (head == NULL) printf("\N List is empty");
	while (head!= NULL)
	{
		PrintProduct(head->p);
		head = head->next;
	}
}

/*LIFO-> LAST IN FIRST OUT, YOU ADD ELEMENTS AT THE BEGINING YOU DELETE AT THE BEGGINING
  FIFO-> FIRST IN LAST OUT, YOU ADD ELEMENTS AT THE END , YOU DELETE AT THE BEGGING
  QUEUE-> YOU DELETE THE ELEMENTS WHEN YOU PRINT THEM, YOU CAN CREATE A PRINTING METHOD THAT DELETS AN ELEMENT AFTER PRINTING.
  RINGBUFFER-> A NORMAL LIST, BUT LAST ELEMENTS POINTS TO THE FIRST ONE.
*/



/*RING BUFFER*/

void insertInRingBUffer(node*&buffer, Product P)
{
	node* newNode = createNode(P);
	if (buffer == NULL)
	{
		buffer = newNode;
		buffer->next = buffer;
	}
	else
	{
		node* position = buffer;
		node*iterator = buffer;
		while (iterator->next != position)
		{
			iterator = iterator->next;
		}
		iterator->next = newNode;
		newNode->next = position;
	}
}
/* Any element in the ring Buffer can be the first or the last you can choose what do you want to delete by playing with *next, up to you to be honest,
i always prefer to delete either the first elemenet or the last element.
*/
void deleteFromRingBuffer(node*&head)
{
	if (head == NULL)
	{
		printf("\nNothing to delete");
	}
	else
	{
		node* firstElement = head;
		node*iterator = head;
		while (iterator->next->next != firstElement)
		{
			iterator = iterator->next;
		}
		node* deleteing = iterator->next;
		iterator->next = firstElement;
		free(deleteing);
		
	}
}
void printRingBuffer(node* buffer)
{
	node* position = buffer;
	
	while (buffer->next != position)
	{
		PrintProduct(buffer->p);
		buffer = buffer->next;
	}
	PrintProduct(buffer->p);
}

/* if you want to print and delete Elements at the same time you just combine the printing and the delete together*/
//Print while delete for a normal simple linked list.
void PrintWhileDeletingElements(node* &head)
{

	if (head == NULL)
	{
		printf("\n List is Empty");
	}

	else
	{
		node* iterator = head;
		
		while (iterator!= NULL)
		{
			PrintProduct(iterator->p);
			node*element = iterator;
			
			iterator = iterator->next;
			free(element);
			
			
		}
		head = NULL;
		
	}

}

/*Double Linked List, it's the same but we have a *node prev and *node next */

struct DoubleNode
{
	Product p;
	DoubleNode * next;
	DoubleNode * prev;

};

DoubleNode* createDoubleNode(Product p)
{
	DoubleNode* newNode = (DoubleNode*)malloc(sizeof(DoubleNode));
	newNode->next = NULL;
	newNode->prev = NULL;
	newNode->p.id = p.id;
	newNode->p.name = new char[strlen(p.name)];
	strcpy(newNode->p.name, p.name);
	newNode->p.price = p.price;
	return newNode;
}

void InsertElementsAtTheEndOfDoubleLinkedList(DoubleNode*&head, Product p)
{
	DoubleNode* newNode = createDoubleNode(p);
	if (head == NULL)
	{
		head = newNode;
	}
	else
	{
		DoubleNode*Iterator = head;
		while (Iterator->next != NULL)
		{
			Iterator = Iterator->next;
		}
		newNode->prev = Iterator;
		Iterator->next = newNode;

	}
}
void InsertElementsAtTheFrontOfDoubleLinkedList(DoubleNode*&head, Product p)
{
	DoubleNode* newNode = createDoubleNode(p);
	if (head == NULL)
	{
		head = newNode;
	}
	else
	{
		
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}

}

void PrintDoubleList(DoubleNode*head)
{
	if (head == NULL)
	{
		printf("\n Nothing to print");
	}
	else
	{
		DoubleNode* iterator = head;
		while (iterator!=NULL)
		{
			PrintProduct(iterator->p);
			iterator = iterator->next;
		}
	}
}

void DeleteElementFromDoubleLinkedListAtTheEnd(DoubleNode*&head)
{
	if (head == NULL)

	{
		printf("\n Nothing to delete");
	}
	if (head->next == NULL)
	{
		free(head);
		head = NULL;
	}
	else
	{
		DoubleNode*Iterator = head;
		while (Iterator->next != NULL)
			Iterator = Iterator->next;
		Iterator->prev->next = NULL;
		//Iterator->next = NULL;
		Iterator->prev = NULL;
		free(Iterator);
	}
}
/*
void DeleteElAtTheBeginingFromDoubleLinkedList(DoubleNode* head)
{
	if (head == NULL)
	{
		printf("\nNothing to delete");
	}
	else if (head->next == NULL)
	{
		free(head);
		head = NULL;
	}
	else
	{
		head->next->prev = NULL;
		DoubleNode*Iterator = head;
		head = head->next;
		head->prev->next = NULL;
		free(Iterator);
	}
}

Not working , can be fixed but I'm bored already so fix it yourselfs;)
*/



/*HASHTABLE*/

struct HashTable
{
	node**keyIndex;
	int noElements;
};

HashTable CreateHashTable(int no)
{
	HashTable myHash;
	myHash.keyIndex = (node**)malloc(no*sizeof(node*));
	for (int i = 0; i < no; i++)
		myHash.keyIndex[i] = NULL;
	myHash.noElements = no;
	return myHash;
}
int getHashValue(float price, int noElements)
{
	return (int)price % noElements;
}

void insertIntoHash(HashTable &myHash, Product &P)
{
	int index = getHashValue(P.price, myHash.noElements);
	node * hashList = myHash.keyIndex[index];
	InsertElementAtTheEndWith(hashList, P);
	myHash.keyIndex[index] = hashList;
}
Product searchProduct(node* head, float price)
{
	if (head == NULL)
	{
		Product dummy = InitProduct(0, "not found", 0);
		return dummy;
	}
	else
	{
		while (head->next != NULL)
		{
			if (head->p.price == price) return head->p;
			head = head->next;
		}
	}
}

Product ReturnProduct(HashTable myHash, float Price)
{
	int index = getHashValue(Price, myHash.noElements);
	return searchProduct(myHash.keyIndex[index], Price);
}






/*RECAPITULARE HASHTABLE*/
/*
struct HashTable {
	node* *keyIndex;
	int noElements;
};
HashTable CreateHashTable(int NoElements)
{
	HashTable myHash;
	myHash.noElements = NoElements;
	myHash.keyIndex = (node**)malloc(NoElements*sizeof(node*));
	for (int i = 0; i < NoElements; i++)
		myHash.keyIndex[i] = NULL;
	return myHash;
}

int getHashValue(int id, int NoElements)
{
	return id % NoElements;
}
void insertIntoHash(HashTable &myHash, Product &P)
{
	int index = getHashValue(P.id, myHash.noElements);
	node*& mylist = myHash.keyIndex[index];
	InsertElementAtTheEndWith(mylist, P);
	myHash.keyIndex[index] = mylist;
}

Product searchProduct(node* head, int id)
{
	Product noFound=InitProduct(0, "Not FOUND", 0);
	if (head == NULL) return noFound;
	else
	{
		while (head->next != NULL)
		{
			if (head->p.id = id) {
				printf("tsus");
			}
			head = head->next;
		}


	}
}
Product returnProduct(HashTable myHash, int id)
{
	int index = getHashValue(id,myHash.noElements);
	return searchProduct(myHash.keyIndex[index], id);
}


*/




/*DATA STRUCTES PART 2*/


//1. BST-> BINARY SEARCH TREE
/*
In a binary search tree any element that is > root goes right, < root goes left
NODE, INSERT, SEARCH , DELETE

*/

struct BSTNode
{
	Product prod;
	BSTNode *left;
	BSTNode *right;
};

BSTNode* createBSTNode(Product P)
{
	BSTNode * newNode = (BSTNode*)malloc(sizeof(BSTNode));
	newNode->prod.id = P.id;
	newNode->prod.price = P.price;
	newNode->prod.name = new char[strlen(P.name)];
	strcpy(newNode->prod.name, P.name);
	newNode->left = NULL;
	newNode->right = NULL;
	return newNode;
}


void InsertIntoBST(BSTNode*&head, Product P)
{
	BSTNode* newNode = createBSTNode(P);
	if (head == NULL)
	{
		head = newNode;
	}
	else if (head->prod.price > newNode->prod.price) InsertIntoBST(head->left, P);
	else if (head->prod.price <= newNode->prod.price) InsertIntoBST(head->right, P);
}

Product SearchProductInBST(BSTNode* head, float price)
{
	Product dummy = InitProduct(0, "NOT FOUND", 0);
	if (head == NULL)
	{
		
		return dummy;
	}
	else if (head->prod.price == price) return head->prod;
	else if (price>(head->prod.price)) SearchProductInBST(head->right, price);
	else SearchProductInBST(head->left, price);

	return dummy;
}

BSTNode * findMinim(BSTNode*head)
{	
	if (head == NULL) return head;
	BSTNode* current = head;
	while (current->left != NULL) current = current->left;
	return current;
}
void deleteFromBST(BSTNode*&node, Product P)
{
	if (node == NULL)
	{
		printf("The BST is Already Empty");
	}
	else if (P.price < node->prod.price) deleteFromBST(node->left, P);
	else if (P.price > node->prod.price) deleteFromBST(node->right, P);
	else
	{
		if (node->left == NULL && node->right == NULL)
		{
			free(node);
			node = NULL;
		}
		else if (node->left == NULL && node->right != NULL)
		{
			BSTNode * current = node;
			node = node->right;
			free(current);
		}
		else if (node->right == NULL && node->left != NULL)
		{
			BSTNode* current = node;
			node = node->left;
			free(current);
		}
		else
		{
			BSTNode * current = findMinim(node->right);
			node->prod.id = current->prod.id;
			node->prod.price = current->prod.price;
			node->prod.name = new char[strlen(current->prod.name)];
			strcpy(node->prod.name, current->prod.name);
			deleteFromBST(node->right, current->prod);

		}

	}
}
void PrintBinaryTree(BSTNode* head)
{
	if (head == NULL)

	{
		printf("\n Tree is Empty");
	}
	else
	{
		if (head->left != NULL) PrintBinaryTree(head->left);
		PrintProduct(head->prod);
		if (head->right != NULL) PrintBinaryTree(head->right);
	}
}
/*AVL IMPLEMENTATION, A TREE THAT MUST BE BALANCED*/

struct AVLNode
{
	Product prod;
	AVLNode*left;
	AVLNode*right;
	int index;
};
AVLNode* createAVLNode(Product P)
{
	AVLNode * newNode = (AVLNode*)malloc(sizeof(AVLNode));
	newNode->left = NULL;
	newNode->right = NULL;
	newNode->prod.id = P.id;
	newNode->prod.price = P.price;
	newNode->prod.name = new char[strlen(P.name)];
	strcpy(newNode->prod.name, P.name);
	newNode->index = 1;
	return newNode;
}

int max(int a, int b)
{
	if (a > b) return a;
	else return b;
}
int getIndex(AVLNode* node)
{
	if (node == NULL) return 0;
	return node->index;
}
AVLNode* rotateRight(AVLNode* &node)
{
	AVLNode* rotation = node->left;
	AVLNode* temporalT = rotation->right;

	rotation->right = node;
	node->left = temporalT;
	node->index = max(getIndex(node->left), getIndex(node->right));
	rotation->index= max(getIndex(rotation->left), getIndex(rotation->right));
	return rotation;
	
}

AVLNode* rotationLeft(AVLNode*& node)
{
	AVLNode* rotation = node->right;
	AVLNode* temporalT = rotation->left;

	rotation->left = node;
	node->right = temporalT;
	node->index = max(getIndex(node->left), getIndex(node->right));
	rotation->index = max(getIndex(rotation->left), getIndex(rotation->right));
	return rotation;
	
}

int CheckBalance(AVLNode* node)
{
	if (node == NULL) return 0;
	else return getIndex(node->left) - getIndex(node->right);


}
AVLNode * findMinimAVL(AVLNode*head)
{
	if (head == NULL) return head;
	AVLNode* current = head;
	while (current->left != NULL) current = current->left;
	return current;
}

void insertInAVL(AVLNode* &top,Product P)
{
	AVLNode* newNode = createAVLNode(P);
	if (top == NULL)
	{
		top = newNode;
		
		
		
	}
	else if (top->prod.price > P.price)insertInAVL(top->left, P);
	else if (top->prod.price < P.price) insertInAVL(top->right, P);
	
	top->index = 1 + max(getIndex(top->left), getIndex(top->right));
	int balance = CheckBalance(top);
	if (balance > 1 && P.price < top->left->prod.price)
	{
		top=rotateRight(top);
		
	}
	if (balance<-1 && P.price>top->right->prod.price)
	{
		top=rotationLeft(top);
		
	}
	if (balance > 1 && P.price > top->left->prod.price)
	{
	top=	rotationLeft(top->left);
		top=rotateRight(top);
		
	}
	if (balance < -1 && P.price < top->right->prod.price)
	{
		top=rotateRight(top->right);
		top=rotationLeft(top);
		
	}
}

void printAVL(AVLNode* top)
{
	
	 if(top!=NULL)
	{
		printAVL(top->left);
		
		printAVL(top->right);
		PrintProduct(top->prod);
	}
}
void deleteFromAVL(AVLNode* &node, Product P)
{
	if (node == NULL)
	{
		printf("The BST is Already Empty");
	}
	else if (P.price < node->prod.price) deleteFromAVL(node->left, P);
	else if (P.price > node->prod.price) deleteFromAVL(node->right, P);
	else
	{
		if (node->left == NULL && node->right == NULL)
		{
			free(node);
			node = NULL;
		}
		else if (node->left == NULL && node->right != NULL)
		{
			AVLNode * current = node;
			node = node->right;
			free(current);
		}
		else if (node->right == NULL && node->left != NULL)
		{
			AVLNode* current = node;
			node = node->left;
			free(current);
		}
		else
		{
			AVLNode * current = findMinimAVL(node->right);
			node->prod.id = current->prod.id;
			node->prod.price = current->prod.price;
			node->prod.name = new char[strlen(current->prod.name)];
			strcpy(node->prod.name, current->prod.name);
			deleteFromAVL(node->right, current->prod);

		}

	}
	if (node == NULL)
	{
		return;
	}
	node->index = 1 + max(getIndex(node->left), getIndex(node->right));
	int balance = CheckBalance(node);
	if (balance > 1 && CheckBalance(node->left) >= 0) node = rotateRight(node);
	if (balance > 1 && CheckBalance(node->left) < 0)
	{
		node->left = rotationLeft(node->left);
		node = rotateRight(node);
	}
	if (balance < -1 && CheckBalance(node->right) <= 0)
			node = rotationLeft(node);
	if (balance < -1 && CheckBalance(node->right) > 0)
	{
		node->right = rotateRight(node->right);
		node = rotationLeft(node);
	}
		

}

/* MAX HEAP -> PRIORITY QUEUE*/

//HEAP - PRIORITY QUEUES
//HEAP = BALANCED (equal weight on both sides) BINARY TREE (links to left and right child)
//HEAP used to provide you instant time to min/max and to reduce the complexity in order to get fast the mix/max
//you get messages all the time with different priority
//the maximum is always the first one, getting the value is fast, but inserting not so fast (complexity: log(2)N)
//every time we insert a new value, we compare it to its parent and we swap them if necessary
//when you remove the root, you put the last node instead of the root and compare the root with its children
//important property: the values are always inserted on the last level from left to right
//so we can insert them in an array
//to get the parent: you get the index minus 1 and you divide it by 2
//to get the children: -left: (index*2)+1  ; -right: (index+1)*2

/*
struct Pacient {
	int codPacient;
	char* numePacient;
	char* problema;
	float varsta;
	int prioritate;
};

Pacient initPacient(int codPacient, const char* _numeP, const char* _problema, float varsta, int prio)
{
	Pacient myPacient;
	myPacient.codPacient = codPacient;
	myPacient.numePacient = new char[strlen(_numeP)];
	strcpy(myPacient.numePacient, _numeP);
	myPacient.problema = new char[strlen(_problema)];
	strcpy(myPacient.problema, _problema);
	myPacient.varsta = varsta;
	myPacient.prioritate = prio;
	return myPacient;
}

struct maxHeap
{
	Pacient *p;
	int noP;
	int capacity;
};
maxHeap createHeap(int cap)
{
	maxHeap myHeap;
	myHeap.capacity = cap;
	myHeap.p = (Pacient*)malloc(sizeof(Pacient)*cap);
	myHeap.noP = 0;
	return myHeap;
}
int getParent(int index)
{
	return (index - 1) / 2;
}
int getLeftChild(int index)
{
	return (index * 2) + 1;
}
int getRightChild(int index)
{
	return (index + 1) * 2;
}
void swap(maxHeap heap, int index1, int index2)
{
	Pacient p = heap.p[index1];
	heap.p[index1] = heap.p[index2];
	heap.p[index2] = p;
}
void resize(maxHeap &heap)
{
	Pacient* newArray = (Pacient*)malloc(heap.capacity * 2 * sizeof(Pacient));
	for(int i=0;i<heap.noP;i++)
	{
		newArray[i] = heap.p[i];
	}
	free(heap.p);
	heap.p = newArray;
	heap.capacity *= 2;
}
void insert(maxHeap& heap, Pacient p) //pentru ca s-ar putea sa n-ai unde sa bagi mesajul, si daca nu mai ai capacitate tre sa resize heap-ul si d-aia trebuie &
{
	if (heap.noP == heap.capacity)
		resize(heap);

	//don't put else, because when you want to insert a new message and they are equal, you call only the resize
	heap.p[heap.noP] = p;
	int currentIndex = heap.noP;
	heap.noP += 1;


	while (currentIndex != 0)
	{
		int parentIndex = getParent(currentIndex);
		if (heap.p[parentIndex].prioritate < p.prioritate)
		{
			swap(heap, parentIndex, currentIndex);
			currentIndex = parentIndex; //now you are on your parent location
		}
		else
			break;
	}
}
*/

int main()
{
	printf("Hello World\n");
	Product p1 = InitProduct(1, "Samsung", 42.41);
	PrintProduct(p1);
	Product p2 = InitProduct(2, "Apple", 45.41);
	Product p3 = InitProduct(3, "Huawaei", 49.23);
	
	SimpleLinkedList mySimpleLinkedList = createList();
	 InsertElementAtTheEnd(mySimpleLinkedList, p1);
	 InsertElementAtTheEnd(mySimpleLinkedList, p2);
	InsertElementAtTheEnd(mySimpleLinkedList, p3);
	insertElementAtTheBegging(mySimpleLinkedList, p3);
	
	printf("\n----PRINTING THE LIST------\n");
	PrintList(mySimpleLinkedList);
	printf("\n----PRINTING THE LIST AFTER DELETE----\n");
	deleteEleementAtTheBegging(mySimpleLinkedList);
	
	deleteEmenetAtTheEnd(mySimpleLinkedList);
	
	PrintList(mySimpleLinkedList);


	node* head = NULL;
	InsertElementAtTheEndWith(head, p1);
	InsertElementAtTheEndWith(head, p2);
	InsertElementAtTheEndWith(head, p3);
	InsertElementAtTheBeginingWith(head, p3);
	printf("\n----PRINTING THE LIST------\n");
	printListMethod2(head);
	printf("\n----PRINTING THE LIST AFTER DELETE----\n");
	DeleteElementAtTheBeginingWith(head);
	DeleteElementAtTheEndWith(head);
	

	printListMethod2(head);

	node* RingBuffer = NULL;
	insertInRingBUffer(RingBuffer, p1);
	insertInRingBUffer(RingBuffer, p2);
	insertInRingBUffer(RingBuffer, p3);
	insertInRingBUffer(RingBuffer, p1);
	printf("\n----PRINTING THE RingBuffer------\n");
	printRingBuffer(RingBuffer);
	printf("\n----PRINTING THE RINGBUFFER AFTER DELETE----\n");
	deleteFromRingBuffer(RingBuffer);
	printRingBuffer(RingBuffer);

	printf("\n----PRINTING WHILE DELETING THE ELEMENTS------\n");
	PrintWhileDeletingElements(head);
	printListMethod2(head);


	DoubleNode* doubleList = NULL;
	InsertElementsAtTheEndOfDoubleLinkedList(doubleList, p1);
	InsertElementsAtTheEndOfDoubleLinkedList(doubleList, p2);
	InsertElementsAtTheEndOfDoubleLinkedList(doubleList, p3);
	InsertElementsAtTheFrontOfDoubleLinkedList(doubleList, p3);
	printf("\n----PRINTING DoubleList------\n");
	PrintDoubleList(doubleList);
	printf("\n----PRINTING THE DOUBLE AFTER DELETE----\n");
	DeleteElementFromDoubleLinkedListAtTheEnd(doubleList);
	DeleteElementFromDoubleLinkedListAtTheEnd(doubleList);
	DeleteElementFromDoubleLinkedListAtTheEnd(doubleList);
	DeleteElementFromDoubleLinkedListAtTheEnd(doubleList);
	PrintDoubleList(doubleList);
	printf("\n---HASHTABLE---\n");
	HashTable myHash = CreateHashTable(1000);
	insertIntoHash(myHash, p1);
	insertIntoHash(myHash, p2);
	insertIntoHash(myHash, p3);
	Product getProduct = ReturnProduct(myHash,42.41);
	PrintProduct(getProduct);
	

	printf("------------BST-------------");
	BSTNode *headBST = NULL;
	InsertIntoBST(headBST, p1);
	InsertIntoBST(headBST, p2);
	InsertIntoBST(headBST, p3);

	Product p4 = SearchProductInBST(headBST, 42.41);
	printf("\n");
	PrintProduct(p4);
	printf("----PRINT THE BINARY TREE----\n");
	deleteFromBST(headBST, p3);
	PrintBinaryTree(headBST);
	InsertIntoBST(headBST,p2);
	//PrintBinaryTree(headBST);
	Product p5 = SearchProductInBST(headBST, 42.41);
	InsertIntoBST(headBST, p2);
	InsertIntoBST(headBST, p2);
	InsertIntoBST(headBST, p2);
	InsertIntoBST(headBST, p2);
	InsertIntoBST(headBST, p2);
	PrintBinaryTree(headBST);
	//printf("\n");
	PrintProduct(p5);
	printf("----------------------\n");
	BSTNode* secondtry = NULL;
	InsertIntoBST(secondtry, p1);
	InsertIntoBST(secondtry, p2);
	InsertIntoBST(secondtry, p3);
	InsertIntoBST(secondtry, p3);
	PrintBinaryTree(secondtry);

	printf("\n-----------------\n");

	AVLNode* myAVL = NULL;
	insertInAVL(myAVL, p1);
	insertInAVL(myAVL, p2);
	insertInAVL(myAVL, p3);
	printAVL(myAVL);
	printf("\n--------------AVL AFTER DELETE----------\n");
	deleteFromAVL(myAVL, p2);// ->> not working...
	printAVL(myAVL);

	/*
	printf("\n---------------max heap------------\n");

	Pacient pac1 = initPacient(1, "GIGEL", "CANCER", 34, 5);
	Pacient pac2 = initPacient(2, "ALEXA", "SIFILIS", 31, 3);
	Pacient pac3 = initPacient(2, "MONA", "OBEZITATE", 31, 4);
	maxHeap myHeap = createHeap(10);
	insert(myHeap, pac1);
	insert(myHeap, pac2);
	insert(myHeap, pac3);
	//printf("\nMaximum priority message is: %s", myHeap.p[0].prioritate);

	*/

	

	




	
	
	

}